import {Rectangle} from "../class/rectangle.class";
import {InterlockService} from "./interlock.service";

const COlOR_RECTANGLE = "#ffe0b3";
const COLOR_DRAGGED_RECTANGLE = "#ff3333";
const STROKE_FOR_RECTANGLE_AND_FILL_FOR_TEXT = "#000";
const HEIGHT_RECTANGLE = 100;

export class DragServiceService {

    public rectangles: Array<Rectangle> = [];
    private numberActiveRectangle: number;
    private interlockService: InterlockService = new InterlockService();

    public addRectangle(x: number, y: number, width: number): void {
        let newWidth: number = this.calculateWidthRectangleByCoordinate(width);
        let newX: number = x - (newWidth / 2);
        let newY: number = y - (HEIGHT_RECTANGLE / 2);
        this.rectangles.push(new Rectangle(newX, newY, newWidth, HEIGHT_RECTANGLE));
        this.numberActiveRectangle = this.rectangles.length - 1;
    }

    public getBackgroundColorForRectangle(index: number): string {
        return index === this.numberActiveRectangle ? COLOR_DRAGGED_RECTANGLE : COlOR_RECTANGLE;
    }

    public getStrokeForRectangle(): string {
        return STROKE_FOR_RECTANGLE_AND_FILL_FOR_TEXT;
    }

    public calculateWidthRectangleByCoordinate(value: number): number {
        return value / 5;
    }

    public clickOnRectangle(x: number, y: number): number {
        this.rectangles.forEach((rect: Rectangle, index) => {
            if (this.checkOnClickInRectangle(x, y, rect)) {
                this.numberActiveRectangle = index;
            }
        });
        return this.numberActiveRectangle;
    }

    private checkOnClickInRectangle(x: number, y: number, rect: Rectangle): boolean {
        return x > rect.x && x < rect.x + rect.width && y > rect.y && y < rect.y + rect.height;
    }

    public moveRectangle(x: number, y: number): void {
        if (this.numberActiveRectangle !== undefined) {
            let activeRectangle: Rectangle = this.rectangles[this.numberActiveRectangle];
            activeRectangle.x = x - (activeRectangle.width / 2);
            activeRectangle.y = y - (activeRectangle.height / 2);
            this.interlock();
        }
    }

    public stopMovingRectangle(widthRightPanel: number): void {
        if(widthRightPanel > this.rectangles[this.numberActiveRectangle].x) {
            this.rectangles.splice(this.numberActiveRectangle, 1);
        }
        if (this.numberActiveRectangle !== undefined) {
            this.numberActiveRectangle = undefined;
        }
    }

    public interlock(): void {
        let activeRectangle: Rectangle = this.rectangles[this.numberActiveRectangle];
        let rectanglesWithoutActiveRectangle: Array<Rectangle> = this.getRectWithoutActiveRect();
        this.interlockService.interlock(activeRectangle, rectanglesWithoutActiveRectangle);
    }

    private getRectWithoutActiveRect(): Array<Rectangle> {
        return this.rectangles.filter((rectangle: Rectangle, index: number) => {
            return index !== this.numberActiveRectangle;
        });
    }

}