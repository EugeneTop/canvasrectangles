import {LeftPanel} from "../class/leftPanel.class";
import {Button} from "../class/buttons/button/button.class";
import {COLOR_BUTTONS, COLOR_TEXT_ON_BUTTONS, PADDING_ON_BUTTONS} from "../class/buttons/buttonsConfig";
import {Rectangle} from "../class/rectangle.class";

const START_COORDINATE = 0;

export class DrawServiceService {

    private canvas: HTMLCanvasElement;
    private context:CanvasRenderingContext2D;

    constructor(canvas: HTMLCanvasElement) {
        this.canvas = canvas;
    }

    public createContext(): void {
        this.context = this.canvas.getContext("2d");
        this.context.beginPath();
    }

    public drawAreaLeftPanel(leftPanelParams: LeftPanel): void {
        this.context.fillStyle = leftPanelParams.fill;
        this.context.fillRect(leftPanelParams.x, leftPanelParams.y, leftPanelParams.width, leftPanelParams.height);
    }

    public drawButton(button: Button): void {
        this.context.fillStyle = COLOR_BUTTONS;
        this.context.fillRect(button.x, button.y, button.width, button.height);
        this.context.fillStyle = COLOR_TEXT_ON_BUTTONS;
        this.context.fillText(button.value.toString(), PADDING_ON_BUTTONS.left + button.x, button.y + PADDING_ON_BUTTONS.top)
    }

    public closePath(): void {
        this.context.closePath();
    }

    public saveAndRestoreContext(): void {
        this.context.save();
        this.context.restore();
    }

    public clear(): void {
        this.context.clearRect(START_COORDINATE, START_COORDINATE,
            this.canvas.width, this.canvas.height);
        this.saveAndRestoreContext();
    }

    public drawRectangle(fill: string, stroke: string, rectangle: Rectangle): void {
        this.context.fillStyle = fill;
        this.context.strokeStyle = stroke;
        this.context.strokeRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        this.context.fillRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
    }
}