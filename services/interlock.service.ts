import {Rectangle} from "../class/rectangle.class";

const SNAP_DISTANCE = 20; //Pixels to snap
const DISTANCE_BETWEEN_RECTANGLES = 1;

export class InterlockService {

    public interlock(activeRectangle: Rectangle, rectangles: Array<Rectangle>): void {
        rectangles.forEach((rectangle: Rectangle) => {

            if(this.checkOnIntersectRectangles(activeRectangle, rectangle)) {
                this.interlockWhenIntersectRectangles(activeRectangle, rectangle);
            }

            this.interlockWhenNotIntersectRectangles(activeRectangle, rectangle);
        });
    }

    private interlockWhenNotIntersectRectangles(activeRectangle: Rectangle, currentRectangle: Rectangle): void {
        this.snapRectangleHorizontally(activeRectangle, currentRectangle);
        this.snapRectangleVertically(activeRectangle, currentRectangle);
    }

    private snapRectangleHorizontally(activeRectangle: Rectangle, currentRectangle: Rectangle): void {
        if(Math.abs((activeRectangle.y + activeRectangle.height) - (currentRectangle.y + currentRectangle.height)) < SNAP_DISTANCE) {
            this.snapRectangleByTopAndLeftOrRight(activeRectangle, currentRectangle);
        }

        if(Math.abs(activeRectangle.y - currentRectangle.y) < SNAP_DISTANCE) {
            this.snapRectangleByBottomAndLeftOrRight(activeRectangle, currentRectangle);
        }
    }

    private snapRectangleByTopAndLeftOrRight(activeRectangle: Rectangle, currentRectangle: Rectangle): void {
        if(Math.abs(activeRectangle.x - (currentRectangle.x + currentRectangle.width)) < SNAP_DISTANCE) {
            activeRectangle.x = currentRectangle.x + currentRectangle.width + DISTANCE_BETWEEN_RECTANGLES;
            activeRectangle.y = currentRectangle.y + currentRectangle.height - activeRectangle.height;
        }

        if(Math.abs(activeRectangle.x + activeRectangle.width - currentRectangle.x) < SNAP_DISTANCE) {
            activeRectangle.x = currentRectangle.x - activeRectangle.width - DISTANCE_BETWEEN_RECTANGLES;
            activeRectangle.y = currentRectangle.y + currentRectangle.height - activeRectangle.height;
        }
    }

    private snapRectangleByBottomAndLeftOrRight(activeRectangle: Rectangle, currentRectangle: Rectangle): void {
        if(Math.abs(activeRectangle.x - (currentRectangle.x + currentRectangle.width)) < SNAP_DISTANCE) {
            activeRectangle.x = currentRectangle.x + currentRectangle.width + DISTANCE_BETWEEN_RECTANGLES;
            activeRectangle.y = currentRectangle.y;

        }

        if(Math.abs((activeRectangle.x + activeRectangle.width) - currentRectangle.x) < SNAP_DISTANCE) {
            activeRectangle.x = currentRectangle.x - activeRectangle.width - DISTANCE_BETWEEN_RECTANGLES;
            activeRectangle.y = currentRectangle.y;
        }
    }

    private snapRectangleVertically(activeRectangle: Rectangle, currentRectangle: Rectangle): void {
        if(Math.abs((activeRectangle.x + activeRectangle.width) - (currentRectangle.x + currentRectangle.width)) < SNAP_DISTANCE) {
            this.snapRectangleByLeftAndTopOrBottom(activeRectangle, currentRectangle);
        }

        if(Math.abs(activeRectangle.x - currentRectangle.x) < SNAP_DISTANCE) {
            this.snapRectangleByRightAndTopOrBottom(activeRectangle, currentRectangle);
        }
    }

    private snapRectangleByLeftAndTopOrBottom(activeRectangle: Rectangle, currentRectangle: Rectangle): void {
        if(Math.abs(activeRectangle.y - (currentRectangle.y + currentRectangle.height)) < SNAP_DISTANCE) {
            activeRectangle.x = currentRectangle.x + currentRectangle.width - activeRectangle.width;
            activeRectangle.y = currentRectangle.y + currentRectangle.height + DISTANCE_BETWEEN_RECTANGLES;
        }

        if(Math.abs((currentRectangle.y + currentRectangle.height) - currentRectangle.y) < SNAP_DISTANCE) {
            activeRectangle.x = currentRectangle.x + currentRectangle.width - activeRectangle.width;
            activeRectangle.y = currentRectangle.y - activeRectangle.height - DISTANCE_BETWEEN_RECTANGLES;
        }
    }

    private snapRectangleByRightAndTopOrBottom(activeRectangle: Rectangle, currentRectangle: Rectangle): void {
        if(Math.abs(activeRectangle.y - (currentRectangle.y + currentRectangle.height)) < SNAP_DISTANCE) {
            activeRectangle.x = currentRectangle.x;
            activeRectangle.y = currentRectangle.y + currentRectangle.height + DISTANCE_BETWEEN_RECTANGLES;
        }

        if(Math.abs((activeRectangle.y + activeRectangle.height) - currentRectangle.y) < SNAP_DISTANCE) {
            activeRectangle.x = currentRectangle.x;
            activeRectangle.y = currentRectangle.y - activeRectangle.height - DISTANCE_BETWEEN_RECTANGLES;
        }
    }

    private interlockWhenIntersectRectangles(activeRectangle: Rectangle, currentRectangle: Rectangle): void {
        let distributionX: number = ((currentRectangle.x + currentRectangle.width) / 2) - ((activeRectangle.x + activeRectangle.width) / 2);
        let distributionY: number = ((currentRectangle.y + currentRectangle.height) / 2) - ((activeRectangle.y + activeRectangle.height) / 2);

        if(Math.abs(distributionX) > Math.abs(distributionY)) {
            this.interlockByLeftAndRightWhenIntersectRectangles(activeRectangle, currentRectangle, distributionX);
        } else {
            this.interlockByTopAndBottomWhenIntersectRectangles(activeRectangle, currentRectangle, distributionY);
        }
    }

    private interlockByLeftAndRightWhenIntersectRectangles(activeRectangle: Rectangle, currentRectangle: Rectangle, distribution: number): void {
        if (distribution > 0) {
            activeRectangle.x = currentRectangle.x - activeRectangle.width - DISTANCE_BETWEEN_RECTANGLES;
        } else {
            activeRectangle.x = currentRectangle.x + currentRectangle.width + DISTANCE_BETWEEN_RECTANGLES;
        }
    }

    private interlockByTopAndBottomWhenIntersectRectangles(activeRectangle: Rectangle, currentRectangle: Rectangle, distributionY: number): void {
        if (distributionY > 0) {
            activeRectangle.y = currentRectangle.y - activeRectangle.height - DISTANCE_BETWEEN_RECTANGLES;
        } else {
            activeRectangle.y = currentRectangle.y + currentRectangle.height + DISTANCE_BETWEEN_RECTANGLES;
        }
    }

    private checkOnIntersectRectangles(currentRectangle: Rectangle, activeRectangle: Rectangle): boolean {
        return !(activeRectangle.x > currentRectangle.x + currentRectangle.width ||
            activeRectangle.x + activeRectangle.width < currentRectangle.x ||
            activeRectangle.y > currentRectangle.y + currentRectangle.height ||
            activeRectangle.y + activeRectangle.height < currentRectangle.y);
    }
}