import {LeftPanel} from "./class/leftPanel.class";
import {Button} from "./class/buttons/button/button.class";
import {BUTTONS_CONFIG, COLOR_BUTTONS, COLOR_TEXT_ON_BUTTONS, PADDING_ON_BUTTONS} from "./class/buttons/buttonsConfig";
import {DragServiceService} from "./services/dragService.service";
import {DrawServiceService} from "./services/drawService.service";
import {Rectangle} from "./class/rectangle.class";

class Main {

    private leftPanel: LeftPanel;
    private buttons: Array<Button> = BUTTONS_CONFIG;
    private dragServiceService: DragServiceService = new DragServiceService();
    private drawService: DrawServiceService;
    private canvas: HTMLCanvasElement;

    constructor() {
        this.createCanvas();
        this.drawService = new DrawServiceService(this.canvas);
        this.drawService.createContext();
        this.drawLeftPanel();
        this.drawService.closePath();
        this.addEventsOnCanvas();
    }

    private createCanvas(): void {
        this.canvas = <HTMLCanvasElement> document.getElementById("canvas");
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
    }

    private drawLeftPanel(): void {
        this.leftPanel = new LeftPanel(this.canvas.height);
        this.drawService.drawAreaLeftPanel(this.leftPanel);
        this.drawingButtonsOnRightPanel();
    }

    private drawingButtonsOnRightPanel(): void {
        this.buttons.forEach((button: Button) => {
            this.drawService.drawButton(button);
        });
        this.drawService.saveAndRestoreContext();
    }

    private addEventsOnCanvas(): void {
        this.canvas.addEventListener("mousedown", ev => {
            this.createAndUpdateRectangle(ev);
        });
        this.canvas.addEventListener("mousemove", ev => {
            this.movingRectangle(ev);
        });
        this.canvas.addEventListener("mouseup", () => {
            this.stopMovingRectangle();
        });
    }

    private createAndUpdateRectangle(event: MouseEvent): void {
        this.clickOnTheButton(event);
        this.clickOnTheRectangle(event);
    }

    private stopMovingRectangle(): void {
        this.dragServiceService.stopMovingRectangle(this.leftPanel.width);
        this.reDraw();
    }

    private movingRectangle(event: MouseEvent): void {
        this.dragServiceService.moveRectangle(event.x, event.y);
        this.reDraw();
    }

    private clickOnTheRectangle(event: MouseEvent): void {
        if(this.dragServiceService.clickOnRectangle(event.x, event.y) !== undefined) {
            this.reDraw();
        }
    }

    private clickOnTheButton(event: MouseEvent): void {
        let clickButton = this.buttons.filter((button: Button) => {
            return this.checkOnPositionButton(event, button);
        });
        if(clickButton.length > 0) {
            this.dragServiceService.addRectangle(event.x, event.y, clickButton[0].value);
            this.reDraw();
        }
    }

    private checkOnPositionButton(event: MouseEvent, button: Button): boolean {
        return event.x > button.x && event.x < button.x + button.width && event.y > button.y && event.y < button.y + button.height;
    }

    private reDraw(): void {
        this.drawService.clear();
        this.drawLeftPanel();
        this.drawRects();
    }

    private drawRects(): void {
        this.dragServiceService.rectangles.forEach((rectangle: Rectangle, index: number) => {
            let fill: string = this.dragServiceService.getBackgroundColorForRectangle(index);
            let stroke: string = this.dragServiceService.getStrokeForRectangle();
            this.drawService.drawRectangle(fill, stroke, rectangle);
        });
    }
}

window.onload = function () {
    let main: Main = new Main();
};


