const PARAMS_FOR_LEFT_PANEL = {
    x: 0,
    y: 0,
    width: 500,
    fill: "#e4e4e4"
};

export class LeftPanel {
    public x: number;
    public y: number;
    public width: number;
    public height: number;
    public fill: string;

    public constructor(height: number) {
        this.x = PARAMS_FOR_LEFT_PANEL.x;
        this.y = PARAMS_FOR_LEFT_PANEL.y;
        this.width = PARAMS_FOR_LEFT_PANEL.width;
        this.height = height;
        this.fill = PARAMS_FOR_LEFT_PANEL.fill;
    }
}