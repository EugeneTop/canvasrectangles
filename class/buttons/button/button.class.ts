const parametersForButton = {
    width: 140,
    height: 50
};

export class Button {

    public x: number;
    public y: number;
    public width: number;
    public height: number;
    public value: number;

    public constructor(x: number, y: number, value: number) {
        this.x = x;
        this.y = y;
        this.width = parametersForButton.width;
        this.height = parametersForButton.height;
        this.value = value;
    }
}