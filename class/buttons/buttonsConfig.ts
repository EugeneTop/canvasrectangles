import {Button} from "./button/button.class";

export const COLOR_TEXT_ON_BUTTONS = "#000000";
export const COLOR_BUTTONS = "#ffffff";
export const PADDING_ON_BUTTONS = {
    top: 30,
    left: 15
};
export const BUTTONS_CONFIG = [
    new Button(30, 100, 150), new Button(180, 100, 200), new Button(330, 100, 250),
    new Button(30, 155, 300), new Button(180, 155, 350), new Button(330, 155, 400),
    new Button(30, 210, 450), new Button(180, 210, 500), new Button(330, 210, 550),
    new Button(30, 265, 600), new Button(180, 265, 650), new Button(330, 265, 700),
    new Button(30, 320, 750), new Button(180, 320, 800), new Button(330, 320, 850),
    new Button(30, 375, 900), new Button(180, 375, 950), new Button(330, 375, 1000)
];